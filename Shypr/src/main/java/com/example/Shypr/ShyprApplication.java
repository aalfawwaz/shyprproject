package com.example.Shypr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShyprApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShyprApplication.class, args);
	}

}
