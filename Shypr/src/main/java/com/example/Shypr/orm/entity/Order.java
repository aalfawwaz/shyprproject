package  com.example.Shypr.orm.entity;
        import javax.persistence.Column;
        import javax.persistence.Entity;
        import javax.persistence.GeneratedValue;
        import javax.persistence.GenerationType;
        import javax.persistence.Id;
        import javax.persistence.Table;

@Entity
@Table(name = "order")
public class Order {

    private long id;
    private int sourceCity;
    private int destinationCity;
    private int carrierId;
    private String paymentFeesOptions;
    private String costLiability;


    public Order() {

    }

    public Order(int sourceCity,int destinationCity,int carrierId, String paymentFeesOptions, String costLiability) {
        this.sourceCity = sourceCity;
        this.destinationCity = destinationCity;
        this.carrierId = carrierId;
        this.paymentFeesOptions = paymentFeesOptions;
        this.costLiability = costLiability;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "source_city", nullable = false)
    public int getSourceCity() {
        return sourceCity;
    }

    public void setSourceCity(int sourceCity) {
        this.sourceCity = sourceCity;
    }

    @Column(name = "destination_city", nullable = false)
    public int getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(int destinationCity) {
        this.destinationCity = destinationCity;
    }

    @Column(name = "carrier_id", nullable = false)
    public int getCarrierId() {
        return carrierId ;
    }

    public void setCarrierId(int carrierId ) {
        this.carrierId = carrierId;
    }

    @Column(name = "payment_fees_options", nullable = false)
    public String getPaymentFeesOptions() {
        return paymentFeesOptions;
    }

    public void setPaymentFeesOptions(String paymentFeesOptions) {
        this.paymentFeesOptions = paymentFeesOptions;
    }

    @Column(name = "cost_liability", nullable = false)
    public String getCostLiability() {
        return costLiability;
    }

    public void setCostLiability(String costLiability) {
        this.costLiability = costLiability ;
    }

    @Override
    public String toString() {
        return "Order [id=" + id +
                ", sourceCity=" + sourceCity +
                ", destinationCity=" + destinationCity +
                ", carrierId=" + carrierId +
                ", paymentFeesOptions=" + paymentFeesOptions +
                ", costLiability=" + costLiability +
                "]";
    }

}
