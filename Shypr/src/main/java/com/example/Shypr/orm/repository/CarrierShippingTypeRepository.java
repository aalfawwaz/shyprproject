package  com.example.Shypr.orm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Shypr.orm.entity.CarrierShippingType;

@Repository
public interface CarrierShippingTypeRepository extends JpaRepository<CarrierShippingType, Long>{

}
