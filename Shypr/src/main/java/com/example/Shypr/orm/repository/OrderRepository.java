package  com.example.Shypr.orm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Shypr.orm.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{

}
