package  com.example.Shypr.orm.entity;
import javax.persistence.Column;
        import javax.persistence.Entity;
        import javax.persistence.GeneratedValue;
        import javax.persistence.GenerationType;
        import javax.persistence.Id;
        import javax.persistence.Table;

@Entity
@Table(name = "carrier_shipping_type")
public class CarrierShippingType {

    private long id;
    private String name;
    private double cost;
    private String estimationDays;

    public CarrierShippingType() {

    }

    public CarrierShippingType(String name,double cost, String estimationDays)
    {
        this.name=name;
        this.cost=cost;
        this.estimationDays=estimationDays;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "cost", nullable = false)
    public double getCost() {
        return cost;
    }
    public void setCost(double cost) {
        this.cost = cost;
    }

    @Column(name = "estimationDays", nullable = false)
    public String getEstimationDays() {
        return estimationDays;
    }
    public void setEstimationDays(String estimationDays) {
        this.estimationDays = estimationDays;
    }

    @Override
    public String toString() {
        return "Carrier [id=" + id +
                ", name=" + name +
                ", cost=" + cost +
                ", estimationDays=" + estimationDays +
                "]";
    }

}
