package  com.example.Shypr.orm.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "city_shipping_restriction")
public class CityShippingRestriction implements Serializable {

    private long id;
    private int sourceCity;
    private int destinationCity;
    private boolean isCodAcceptable;

    public CityShippingRestriction() {

    }

    public CityShippingRestriction(int sourceCity,int destinationCity,boolean isCodAcceptable) {
        this.sourceCity = sourceCity;
        this.destinationCity = destinationCity;
        this.isCodAcceptable=isCodAcceptable;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }


    @Column(name = "source_city", nullable = false)
    public int getSourceCity() {
        return sourceCity;
    }

    public void setSourceCity(int sourceCity) {
        this.sourceCity = sourceCity;
    }

    @Column(name = "destination_city", nullable = false)
    public int getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(int destinationCity) {
        this.destinationCity = destinationCity;
    }

   @Column(name = "is_cod_acceptable", nullable = false)
    public boolean getIsCodAcceptable() {
        return isCodAcceptable;
    }

    public void setIsCodAcceptable(boolean isCodAcceptable) {
        this.isCodAcceptable = isCodAcceptable;
    }

    @Override
    public String toString() {
        return "CityShippingRestriction [id=" + id +
                ", sourceCity=" + sourceCity +
                ", destinationCity=" + destinationCity +
                ", isCodAcceptable=" + isCodAcceptable +
                "]";
    }

}
